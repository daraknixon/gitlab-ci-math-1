#!/bin/sh

#Kamaria Nixon
#1702413

mkdir -p build


# read -p 'Would you care to change the name of the file? Y/N? ' change
# if [[ "$change" == "Y" ] || ["$change" == "y" ]]; then

    # extent=".txt"
    # read -p "What would you like your file to be called? "  OUTPUT_FILE_NAME
    # OUTPUT_FILE_NAME="$OUTPUT_FILE_NAME$extent"

    # echo  ""  


if [[ -z "$OUTPUT_FILE_NAME" ]]; then
    OUTPUT_FILE_NAME="myresult.txt"
    echo "Name not changed. The result will be saved to the default destination $OUTPUT_FILE_NAME"
fi

touch build/$OUTPUT_FILE_NAME


num1=$1
num2=$2
num3=$3

result=$(( ($num1**$num2)+ $num3 * $num2 ))
echo $result > build/$OUTPUT_FILE_NAME
echo "This progam prints the result of ($num1 ^ $num2) + $num3 x $num2"
echo "The result is: $result"
